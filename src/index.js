import React from 'react';
import {render} from 'react-dom';
import { MyProfile } from "./components/myprofile";
import { Callback } from "./components/callback";
import {BrowserRouter , Link , Route , Switch} from 'react-router-dom';


const CLIENT_ID = 'bf4oel67cupb111k67f0';
const RESPONSE_TYPE = 'token';
const REDIRECT_URI = 'http://localhost:3000/cb';
const SCOPE = 'read write';
const AUTH_URL = `http://localhost:9090/oauth/auth?client_id=${CLIENT_ID}&response_type=${RESPONSE_TYPE}&redirect_uri=${REDIRECT_URI}&scope=${SCOPE}`;


const NotLoggedIn = (props) => (
    <a href={props.authUrl}>Login with oauth provider</a>
)

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {accessToken: null};
        this.loginState = this.loginState.bind(this);
    }


    loginState(){
        
    }


    render(){
        return (
            <Switch>
                <Route path="/myprofile" component={MyProfile}/>
                <Route path="/cb" component={Callback}/>
                <Route path="/" exact render={()=>(
                    <NotLoggedIn authUrl={this.props.authUrl}/>
                )}/>
            </Switch>
        );
    }
}

render(
    <BrowserRouter>
        <App
            authUrl= {AUTH_URL}
        />
    </BrowserRouter>, 
    document.getElementById('root')
);