import React from 'react';


export class MyProfile extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            username:null,
            password:null,
        };
    }

    componentDidMount(){
        fetch(`http://localhost:9600/user/${localStorage.getItem('username')}`,{
            method:'GET',
            headers:{
                'Authorization':`Bearer ${localStorage.getItem('access_token')}`
            }
        })
            .then(r=> r.json())
            .then(rJson=>{
                const {data, code} = rJson;

                if (code !== 200){
                    window.location.href = '/';
                    localStorage.clear();
                    return
                }

                this.setState({...data});
            })
            .catch(err=>{
                window.location.href = '/';
                localStorage.clear();

                console.error(err);
            })
    }
    render(){
        return (
            <div>
                <div>Profile...</div>
                <div>Username : {this.state.username}</div>
                <div>Password : {this.state.password}</div>
            </div>
        )
    }
}