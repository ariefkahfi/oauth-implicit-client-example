import React from 'react';

export class Callback extends React.Component {

    constructor(props){
        super(props);
        this.state = {isLoadingMessage: "Authorizing..."};
    }

    componentDidMount(){
        console.log('on didMount...');
        const locHash = window.location.hash;
        const firstEqual = locHash.indexOf('=');
        const firstAmper = locHash.indexOf('&');

        const accessToken = locHash.substring(firstEqual + 1,firstAmper);
        console.log(accessToken);

        // get access_token
        // hit /oauth/introspect
        // to get username from token
        // ...

        let formData = new FormData();
        formData.append('token',accessToken);

        fetch(`http://localhost:9090/oauth/introspect`,{
            method:'POST',
            body:formData,
        })
            .then(r=> r.json())
            .then(rJson=> {
                localStorage.setItem('access_token',accessToken);
                localStorage.setItem('username',rJson.username);

                window.location.href = '/myprofile';
            })
            .catch(err=>{
                console.error(err);
            });
                
        
        // soon
        // update state with react context api
        // so we can use it across component
        // ...
    }
    render(){
        return (
            <div>{this.state.isLoadingMessage}</div>
        );
    }
}